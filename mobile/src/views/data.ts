import { AnyTxtRecord } from 'dns';

export const image_list = [
  {
    image_url:
      'https://boot-img.xuexi.cn/image/1005/process/bf41d8f73d284a74b2edcabe44f4afdf.jpg',
    title: '在中国共产党第二十次全国代表大会上的报告',
    link: 'https://article.xuexi.cn/articles/pdf/index.html?art_id=10508589428261528875&reader=d&item_id=10508589428261528875'
  },
  {
    image_url:
      'https://boot-img.xuexi.cn/image/1005/process/21e3a5bcba8f4c17a129eb94612f0495.jpg',
    title: '中国共产党章程',
    link: 'https://article.xuexi.cn/articles/pdf/index.html?art_id=2651078256595357219&reader=d&item_id=2651078256595357219'
  },
  {
    image_url:
      'https://boot-img.xuexi.cn/image/1005/process/0966e1107cf141f0b6f02ccf189e4787.jpg',
    title: '中国共产党第二十次全国代表大会文件汇编',
    link: 'https://article.xuexi.cn/articles/pdf/index.html?art_id=2485261545699073166&reader=d&item_id=2485261545699073166'
  }
];

export const meeting_list = [
  {
    title: '《中国共产党第二十次全国代表大会关于十九届中央委员会报告的决议》',
    link: 'https://www.xuexi.cn/lgpage/detail/index.html?id=12339339023432693693&amp;item_id=12339339023432693693'
  },
  {
    title:
      '《中国共产党第二十次全国代表大会关于十九届中央纪律检查委员会工作报告的决议》',
    link: 'https://www.xuexi.cn/lgpage/detail/index.html?id=1526049386115659259&amp;item_id=1526049386115659259'
  },
  {
    title:
      '《中国共产党第二十次全国代表大会关于《中国共产党章程（修正案）》的决议》',
    link: 'https://www.xuexi.cn/lgpage/detail/index.html?id=13490633588991017369&amp;item_id=13490633588991017369'
  }
];

export const tools_list = [
  {
    id: '0',
    title: '二十大报告学习手账+思维导图',
    link: 'https://mp.weixin.qq.com/s/elBnojPhT9KHxKve0pGXpA',
    type: 'link',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '1',
    title: '一图速览二十大报告',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/RlFePdqL3YwBurIleKQIhA',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '2',
    title: '二十大报告中的中国古语',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/aHqtre-kyXvrPnQrFAVTyw',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '3',
    title: '9个字细读党的二十大报告',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/VIPilcs_-gzyDL9ctL5h8g',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '4',
    title: '9个数字速读二十大报告',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/DyZkR3WXPD_iaHCltbRaTg',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '5',
    title: '党的二十大报告提到了哪些精神',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/fOgdXXZYAXY7UY8-n3r0aQ',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '6',
    title: '《中国共产党章程》修改对比',
    type: 'link',
    link: 'https://mp.weixin.qq.com/s/cr1q_oYUXu2p2CuQWZS2hg',
    content: [
      { type: 'text', content: '' },
      { type: 'image', content: '' }
    ]
  },
  {
    id: '7',
    title: '党的二十大报告中的精神简介',
    type: 'inner',
    link: 'https://mp.weixin.qq.com/s/cr1q_oYUXu2p2CuQWZS2hg',
    content: [
      { type: 'text', content: '伟大建党精神', subtitle: true },
      { type: 'text', content: '大会的主题是：高举中国特色社会主义伟大旗帜，全面贯彻新时代中国特色社会主义思想，弘扬伟大建党精神，自信自强、守正创新，踔厉奋发、勇毅前行，为全面建设社会主义现代化国家、全面推进中华民族伟大复兴而团结奋斗。' },

      { type: 'text', content: '钉钉子精神', subtitle: true },
      { type: 'text', content: '我们持之以恒正风肃纪，以钉钉子精神纠治“四风”，反对特权思想和特权现象，坚决整治群众身边的不正之风和腐败问题，刹住了一些长期没有刹住的歪风，纠治了一些多年未除的顽瘴痼疾。' },

      { type: 'text', content: '历史自觉和主动精神', subtitle: true },
      { type: 'text', content: '中国人民的前进动力更加强大、奋斗精神更加昂扬、必胜信念更加坚定，焕发出更为强烈的历史自觉和主动精神，中国共产党和中国人民正信心百倍推进中华民族从站起来、富起来到强起来的伟大飞跃。' },

      { type: 'text', content: '斗争精神', subtitle: true },
      { type: 'text', content: '面对国际局势急剧变化，特别是面对外部讹诈、遏制、封锁、极限施压，我们坚持国家利益为重、国内政治优先，保持战略定力，发扬斗争精神，展示不畏强权的坚定意志，在斗争中维护国家尊严和核心利益，牢牢掌握了我国发展和安全主动权。' },

      { type: 'text', content: '社会主义法治精神', subtitle: true },
      { type: 'text', content: '社会主义法治精神，出自习近平总书记在中国共产党第二十次全国代表大会上所作的报告。习近平总书记在二十大报告中强调：加快建设法治社会，弘扬社会主义法治精神，传承中华优秀传统法律文化，引导全体人民做社会主义法治的忠实崇尚者、自觉遵守者、坚定捍卫者，努力使尊法学法守法用法在全社会蔚然成风。' },

      { type: 'text', content: '中国共产党人精神谱系', subtitle: true },
      { type: 'text', content: '广泛践行社会主义核心价值观。社会主义核心价值观是凝聚人心、汇聚民力的强大力量。弘扬以伟大建党精神为源头的中国共产党人精神谱系，用好红色资源，深入开展社会主义核心价值观宣传教育，深化爱国主义、集体主义、社会主义教育，着力培养担当民族复兴大任的时代新人。' },


      { type: 'text', content: '劳动精神、奋斗精神、奉献精神、创造精神、勤俭节约精神', subtitle: true },
      { type: 'text', content: '劳动精神、奋斗精神、奉献精神、创造精神、勤俭节约精神是习近平总书记于2022年下半年展开的二十大，在党的二十大报告中指出的。报告则提出了劳动精神、奋斗精神、奉献精神、创造精神、勤俭节约精神新五种精神，并将这五种精神作为时代新风貌的培育内容和全社会文明建设的精神内涵。' },

      { type: 'text', content: '战斗精神', subtitle: true },
      { type: 'text', content: '习主席指出，人民军队从胜利走向胜利，彰显了战斗精神的伟大力量。回顾人民军队95年辉煌历程，敢于斗争、敢于胜利，一不怕苦、二不怕死，始终是我军克敌制胜的重要法宝。进入新时代，着眼把人民军队全面建成世界一流军队，迫切需要加强新时代战斗精神培育，为提高我军备战打仗能力、实现党在新时代的强军目标提供强大精神动力。' },

      { type: 'text', content: '人民首创精神', subtitle: true },
      { type: 'text', content: '全党要坚持全心全意为人民服务的根本宗旨，树牢群众观点，贯彻群众路线，尊重人民首创精神，坚持一切为了人民、一切依靠人民，从群众中来、到群众中去，始终保持同人民群众的血肉联系，始终接受人民批评和监督，始终同人民同呼吸、共命运、心连心，不断巩固全国各族人民大团结，加强海内外中华儿女大团结，形成同心共圆中国梦的强大合力。' },


      { type: 'image', content: '' }
    ]
  },
  // {
  //     id: '8',
  //     title: '视频',
  //     type: "video",
  //     video_url: "https://cdn.jsdelivr.net/gh/xdlumia/files/video-play/IronMan.mp4",
  //     poster: "https://cdn.jsdelivr.net/gh/xdlumia/files/video-play/ironMan.jpg",
  //     link: "",
  //     content: [
  //         { type: "text", content: "" },
  //         { type: "image", content: "" }
  //     ]
  // },
  {
    id: '9',
    title: '视频：“学习二十大 永远跟党走 奋进新征程”主题云团课',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1544y1D71f/?vd_source=f5a4cb9cf8eb4684eebbae3f0e8f6bf5'
  },
  {
    id: '10',
    title: '视频：二十大精神二十人讲：高质量发展',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1gP4y1m7Fw/?spm_id_from=333.788.recommend_more_video.-1&vd_source=f5a4cb9cf8eb4684eebbae3f0e8f6bf5'
  },
  {
    id: '11',
    title: '视频：二十大精神二十人讲：绿色发展',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1qD4y1477Q/?spm_id_from=333.788.recommend_more_video.1&vd_source=f5a4cb9cf8eb4684eebbae3f0e8f6bf5'
  },
  {
    id: '12',
    title: '视频：二十大精神二十人讲：文化自信自强',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV15G4y1b7hh/?spm_id_from=333.788.recommend_more_video.2&vd_source=f5a4cb9cf8eb4684eebbae3f0e8f6bf5'
  },
  {
    id: '14',
    title: '视频：【二十大精神面对面】让创新处处涌动',
    type: 'link',
    link: 'https://article.xuexi.cn/articles/index.html?art_id=1841416702266017021&item_id=1841416702266017021&study_style_id=video_default&t=1673580961867&showmenu=false&ref_read_id=4e2595d2-02e6-4afd-b44c-76cf32749aea_1673788964979&pid=&ptype=-1&source=share&share_to=wx_single'
  },
  {
    id: '15',
    title: '视频：【二十大精神面对面】让经济高质量发展',
    type: 'link',
    link: 'https://article.xuexi.cn/articles/index.html?art_id=16929945464681701439&item_id=16929945464681701439&study_style_id=video_default&t=1673494688113&showmenu=false&ref_read_id=4e2595d2-02e6-4afd-b44c-76cf32749aea_1673788964979&pid=&ptype=-1&source=share&share_to=wx_single'
  },
  {
    id: '18',
    title:
      '视频：《二十大报告里看未来：如何理解“中国式现代化”》第1集：为什么要走中国式现代化道路？',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV12M411z7Fe/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=669f6fcd-7bc5-4096-9d27-bd6544e6f37a&share_source=WEIXIN&share_tag=s_i&timestamp=1673789647&unique_k=vVhcos9&up_id=488055582&vd_source=ace2822c041a5162d3f4b4fa8f632f87'
  },
  {
    id: '19',
    title:
      '视频：《二十大报告里看未来：如何理解“中国式现代化”》第2集：什么是中国式现代化？',
    type: 'link',
    link: 'https://b23.tv/zGK8ESm?share_medium=android&share_source=weixin&bbid=XY469DF442FFA80A4614D8772519BC9DBC29B&ts=1673789658177'
  },
  {
    id: '20',
    title:
      '视频：《二十大报告里看未来：如何理解“中国式现代化”》第3集：如何走好中国式现代化道路？',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1Jg411E74C/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=29cb49f4-264c-46c0-89ab-4f4a13d66a49&share_source=WEIXIN&share_tag=s_i&timestamp=1673789667&unique_k=FiSClsx&up_id=488055582'
  },
  {
    id: '21',
    title:
      '视频：《二十大报告里看未来：如何理解“中国式现代化”》第4集：为什么说高质量发展是全面建设社会主义现代化国家的是首要任务？',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1Md4y1v7or/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=580fbb26-b4c7-4b3e-93f0-818f6344b3c8&share_source=WEIXIN&share_tag=s_i&timestamp=1673789676&unique_k=eqqbkNP&up_id=488055582'
  },
  {
    id: '22',
    title:
      '视频：《二十大报告里看未来：如何“理解中国式现代化”》第5集：中国式现代化会为世界发展带来什么？',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1pe411w7Wr/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=38b9c5fd-bbb2-4ffa-b36e-21642445b779&share_source=WEIXIN&share_tag=s_i&timestamp=1673789684&unique_k=SgIctnt&up_id=488055582'
  },

  {
    id: '16',
    title: '视频：二十大报告金句视频版',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV13g41187rQ/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=a8a2c625-deac-4a39-bffa-9e22eb1501df&share_source=WEIXIN&share_tag=s_i&timestamp=1673789729&unique_k=Bd4WC0N&up_id=1131457022'
  },
  {
    id: '17',
    title: '视频：解读二十大报告：新论断新表述的导向在哪儿？为民造福！',
    type: 'link',
    link: 'https://www.bilibili.com/video/BV1ze4y1x71q/?buvid=XY469DF442FFA80A4614D8772519BC9DBC29B&is_story_h5=false&mid=eH5rtRf0xHtwxCVBTBFG8w%3D%3D&p=1&share_from=ugc&share_medium=android&share_plat=android&share_session_id=82853a4d-2a2d-4c25-bc5a-0ea5ed24802b&share_source=WEIXIN&share_tag=s_i&timestamp=1673789745&unique_k=V3nt14W&up_id=390917283'
  }
];

export const get_tool_item = (id: number) => {
  return tools_list[id];
};

export const spirit_list = ['伟大建党精神', '钉钉子精神', '历史自觉和主动精神'];

// 报告原文 artcile
// 知识点 tips
// 大学生角度理解 understand
// 大学生可以怎么做 todo

export const spirit_detail = [
  {
    title: '伟大建党精神',
    artcile:
      '<p>大会的主题是：高举中国特色社会主义伟大旗帜，全面贯彻新时代中国特色社会主义思想，弘扬<strong>伟大建党精神</strong>，自信自强、守正创新，踔厉奋发、勇毅前行，为全面建设社会主义现代化国家、全面推进中华民族伟大复兴而团结奋斗。</p>',
    tips: '<p>伟大建党精神是习近平总书记2021年7月1日，在庆祝中国共产党成立100周年大会上首次提出的。会上，习近平总书记将伟大建党精神概括为“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”32个字，并指出，这是中国共产党的精神之源。</p>',
    understand:
      '<p>伟大建党精神的内涵包含四个方面。</p><p>坚持真理、坚守理想，就是追求马克思主义真理，坚持社会主义和共产主义理想信念。一百年前，一群新青年高举马克思主义思想火炬，在风雨如晦的中国苦苦探寻民族复兴的前途。由此，中国共产党应运而生。有了马克思主义科学真理的指导，有了对社会主义和共产主义理想的坚定信念，有了中国共产党的坚强领导，中国革命的面貌就焕然一新，中国人民的精神就从被动转为主动，开启了改天换地的伟大革命，为中华民族的复兴确定了唯一正确的方向。在带领中国人民进行长期的革命、建设和改革的历程中，中国共产党人始终坚持真理、坚守理想，并用马克思主义真理宣传群众、动员群众、组织群众，由此凝聚起亿万人民群众改变中华民族命运的磅礴力量。</p>',
    todo: '<p>对大学生而言，坚持伟大建党精神，即要做到“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”。</p><p>首先，“坚持真理、坚守理想”要学习马克思主义真理，坚持社会主义和共产主义等先进思想，及时学习会议精神，思想上与党中央保持高度一致，并胸怀理想不断向前。</p><p>“践行初心、担当使命”要做到将马克思主义真理运用于改造中国与世界的伟大实践，对大学生来说，就是要将其运用于专业学习与科研创新中。作为计算机科学与技术专业的学生，要敢于创新，关注国家发展需要的核心技术，脚踏实地，滴水石穿，将知识转化为生产力，为建设数字中国而做出自己的贡献，不负时代，不负华年。</p><p>“不怕牺牲、英勇斗争”即为了实现伟大目标、理想要有百折不挠、奋勇拼搏、不怕牺牲的斗争精神。对大学生而言，就是在各个方面都要有英勇斗争的精神。比如在科研上遇到困难，要有斗争精神，不轻言放弃，顽强斗争；又比如在疫情防控工作中，拼搏在前，勇于奉献。</p><p>“对党忠诚、不负人民”即全心全意为人民服务。大学生党员要积极带头参加各项志愿服务活动与社会实践，奉献他人，充实自己；掌握专业知识，为国家建设添砖加瓦。</p><p>在新的历史征程上，我们要认真落实习近平总书记提出的要求：“弘扬光荣传统、赓续红色血脉，永远把伟大建党精神继承下去、发扬光大！”</p>'
  },
  {
    title: '钉钉子精神',
    artcile:
      '大会的主题是：高举中国特色社会主义伟大旗帜，全面贯彻新时代中国特色社会主义思想，弘扬伟大建党精神，自信自强、守正创新，踔厉奋发、勇毅前行，为全面建设社会主义现代化国家、全面推进中华民族伟大复兴而团结奋斗。',
    tips: '伟大建党精神是习近平总书记2021年7月1日，在庆祝中国共产党成立100周年大会上首次提出的。会上，习近平总书记将伟大建党精神概括为“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”32个字，并指出，这是中国共产党的精神之源。',
    understand:
      '<p>伟大建党精神的内涵包含四个方面。</p><p>坚持真理、坚守理想，就是追求马克思主义真理，坚持社会主义和共产主义理想信念。一百年前，一群新青年高举马克思主义思想火炬，在风雨如晦的中国苦苦探寻民族复兴的前途。由此，中国共产党应运而生。有了马克思主义科学真理的指导，有了对社会主义和共产主义理想的坚定信念，有了中国共产党的坚强领导，中国革命的面貌就焕然一新，中国人民的精神就从被动转为主动，开启了改天换地的伟大革命，为中华民族的复兴确定了唯一正确的方向。在带领中国人民进行长期的革命、建设和改革的历程中，中国共产党人始终坚持真理、坚守理想，并用马克思主义真理宣传群众、动员群众、组织群众，由此凝聚起亿万人民群众改变中华民族命运的磅礴力量。</p>',
    todo: '<p>对大学生而言，坚持伟大建党精神，即要做到“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”。</p><p>首先，“坚持真理、坚守理想”要学习马克思主义真理，坚持社会主义和共产主义等先进思想，及时学习会议精神，思想上与党中央保持高度一致，并胸怀理想不断向前。</p>'
  },
  {
    title: '历史自觉和主动精神',
    artcile:
      '大会的主题是：高举中国特色社会主义伟大旗帜，全面贯彻新时代中国特色社会主义思想，弘扬伟大建党精神，自信自强、守正创新，踔厉奋发、勇毅前行，为全面建设社会主义现代化国家、全面推进中华民族伟大复兴而团结奋斗。',
    tips: '伟大建党精神是习近平总书记2021年7月1日，在庆祝中国共产党成立100周年大会上首次提出的。会上，习近平总书记将伟大建党精神概括为“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”32个字，并指出，这是中国共产党的精神之源。',
    understand:
      '<p>伟大建党精神的内涵包含四个方面。</p><p>坚持真理、坚守理想，就是追求马克思主义真理，坚持社会主义和共产主义理想信念。一百年前，一群新青年高举马克思主义思想火炬，在风雨如晦的中国苦苦探寻民族复兴的前途。由此，中国共产党应运而生。有了马克思主义科学真理的指导，有了对社会主义和共产主义理想的坚定信念，有了中国共产党的坚强领导，中国革命的面貌就焕然一新，中国人民的精神就从被动转为主动，开启了改天换地的伟大革命，为中华民族的复兴确定了唯一正确的方向。在带领中国人民进行长期的革命、建设和改革的历程中，中国共产党人始终坚持真理、坚守理想，并用马克思主义真理宣传群众、动员群众、组织群众，由此凝聚起亿万人民群众改变中华民族命运的磅礴力量。</p>',
    todo: '<p>对大学生而言，坚持伟大建党精神，即要做到“坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民”。</p><p>首先，“坚持真理、坚守理想”要学习马克思主义真理，坚持社会主义和共产主义等先进思想，及时学习会议精神，思想上与党中央保持高度一致，并胸怀理想不断向前。</p>'
  },
  {
    title: '斗争精神',
    artcile:
      '面对国际局势急剧变化，特别是面对外部讹诈、遏制、封锁、极限施压，我们坚持国家利益为重、国内政治优先，保持战略定力，发扬斗争精神，展示不畏强权的坚定意志，在斗争中维护国家尊严和核心利益，牢牢掌握了我国发展和安全主动权。',
    tips: '斗争精神，出自习近平总书记在中国共产党第二十次全国代表大会上所作的报告。习近平总书记在报告中强调，坚持发扬斗争精神。增强全党全国各族人民的志气、骨气、底气，不信邪、不怕鬼、不怕压，知难而进、迎难而上，统筹发展和安全，全力战胜前进道路上各种困难和挑战，依靠顽强斗争打开事业发展新天地。',
    understand:
      '<p>习近平总书记在党的二十大报告中强调：“广大青年要坚定不移听党话、跟党走，怀抱梦想又脚踏实地，敢想敢为又善作善成，立志做有理想、敢担当、能吃苦、肯奋斗的新时代好青年”。以中国式现代化全面推进中华民族伟大复兴，前途光明，任重道远，当代青年重任在肩。高校要深入研究青年成长规律和时代特点，把思想政治工作贯穿教育教学全过程，引导青年激发斗争精神，明确斗争方向，增强斗争意志，提高斗争本领，让青春在全面建设社会主义现代化国家的火热实践中绽放绚丽之花。</p>',
    todo: '<p>正如习近平总书记所言，青年要经受严格的思想淬炼、政治历练、实践锻炼，在复杂严峻的斗争中经风雨、见世面、壮筋骨，真正锻造成为烈火真金。经受严格的思想淬炼，就是要求青年要打牢斗争的思想根基。青年必须加强马克思主义理论武装，认真学习习近平新时代中国特色社会主义思想，并严格做到自觉主动学、及时跟进学、联系实际学、笃信笃行学。认真学习马克思主义经典著作，学原文、读原著、悟原理，用马克思主义立场观点方法来研究问题、分析问题、解决问题。认真学习党史、新中国史、改革开放史、社会主义发展史，搞明白我们从哪里来、现在哪里、去向何处，搞明白中国共产党为什么“能”，马克思主义为什么“行”，中国特色社会主义为什么“好”。</p>'
  },
  {
    title: '社会主义法治精神',
    artcile:
      '弘扬社会主义法治精神，传承中华优秀传统法律文化，引导全体人民做社会主义法治的忠实崇尚者、自觉遵守者、坚定捍卫者。',
    tips: '社会主义法治精神，出自习近平总书记在中国共产党第二十次全国代表大会上所作的报告。习近平总书记在二十大报告中强调：加快建设法治社会，弘扬社会主义法治精神，传承中华优秀传统法律文化，引导全体人民做社会主义法治的忠实崇尚者、自觉遵守者、坚定捍卫者，努力使尊法学法守法用法在全社会蔚然成风。',
    understand:
      '<p>“奉法者强则国强，奉法者弱则国弱。”法治强调人们对法律的信仰和对既有法制体系的尊重和认同，是社会主义核心价值观的重要内容。我国法治建设经验表明，既要加强制度建设，为新时代中国特色社会主义奠定制度基础，也要推动法治精神建设，让法治成为当代中国的一种信仰。</p><p>社会主义法治精神，是坚持党的领导的法治精神。</p><p>社会主义法治精神，是坚持以人民为中心的法治精神。</p><p>社会主义法治精神，也是一种为全体中国人所普遍感知和认同的精神力量。</p>',
    todo: '<p>大学生是社会主义法治国家建设的重要力量，树立社会主义法治观念，关系到我国社会主义法治的建设，所以对于我们来说，树立社会主义法治观念是很有必要的。对于如何践行社会主义法治精神，可以分为以下几点:</p><p>一是坚持中国共产党的领导。</p><p>二是树立正确的法律自由平等观念。</p><p>三是树立正确的法律公正概念。</p><p>四是树立正确的法律权利与义务观念。</p><p>五是坚持为人民服务，从全局出发。</p>'
  },
  {
    title: '中国共产党人精神谱系',
    artcile:
      '广泛践行社会主义核心价值观。社会主义核心价值观是凝聚人心、汇聚民力的强大力量。弘扬以伟大建党精神为源头的中国共产党人精神谱系，用好红色资源，深入开展社会主义核心价值观宣传教育，深化爱国主义、集体主义、社会主义教育，着力培养担当民族复兴大任的时代新人。',
    tips: '2021年7月1日，习近平总书记在庆祝中国共产党成立100周年大会上概括提出伟大建党精神：坚持真理、坚守理想，践行初心、担当使命，不怕牺牲、英勇斗争，对党忠诚、不负人民。一百年来，党以伟大建党精神为源头，构筑起了中国共产党人的精神谱系，包括井冈山精神、苏区精神、长征精神、遵义会议精神、延安精神、抗战精神、红岩精神、西柏坡精神、抗美援朝精神、“两弹一星”精神、改革开放精神、特区精神、抗洪精神、抗震救灾精神、脱贫攻坚精神、抗疫精神等伟大精神 。2021年9月29日，党中央批准了中央宣传部梳理的第一批纳入中国共产党人精神谱系的伟大精神，在中华人民共和国成立72周年之际予以发布。',
    understand:
      '<p>以伟大建党精神为源头的中国共产党人精神谱系，是我们党立党兴党、执政兴国的宝贵精神财富，是推动党的事业发展的不竭动力，具有超越时空的恒久价值。不断开创新局面的坚强精神支撑。中国共产党之所以能够战胜前进道路上的各种困难和风险，取得中国革命、建设、改革的伟大成就，非常重要的一条就在于弘扬伟大建党精神，铸就了独具特色的精神谱系。今天，我们前所未有地接近中华民族伟大复兴的宏伟目标，但前进道路上的困难和挑战前所未有，这就要求我们不断从中国共产党人精神谱系中汲取力量，为开创新局面提供丰厚精神滋养。</p>',
    todo: '<p>对大学生而言，在新的历史起点上，我们要传承好中国共产党人精神谱系，继续将其融入中国共产党人血脉和基因，为全面建设社会主义现代化国家提供不竭精神动力。不管物质富足还是匮乏，我们都要时刻保持昂扬向上的精气神。</p><p>我们要继续保持和发扬中国共产党人的优良传统，经受住享乐主义等有害思想的侵蚀，在生活条件大大改善的情况下，不丢掉自力更生、艰苦奋斗的传家宝。学习先进做法，转化具体行动。</p>'
  }
  // <p></p>
];

export const get_detail_item = (id: number) => {
  return spirit_detail[id];
};
