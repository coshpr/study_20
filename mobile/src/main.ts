import { createApp } from 'vue';
import App from './App.vue';
import { Button, Icon, Swipe, SwipeItem } from 'vant';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import './styles/index.scss';

import router from './router';

dayjs.locale('zh-ch');

const app = createApp(App);
app.config.globalProperties.$dayjs = dayjs;
app.use(Button, Icon, Swipe, SwipeItem);
app.use(router);
app.mount('#app');
